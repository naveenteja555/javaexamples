package com.db.application;

public interface IApplication {

    /**
     *
     * @return string
     */
    String getVersion();

    double deposit(String accountid,double amount);

    double withdraw(String accountid,double amount);
}
