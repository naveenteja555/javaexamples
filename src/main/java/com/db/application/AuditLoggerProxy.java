package com.db.application;

public class AuditLoggerProxy implements IApplication {
    IApplication target = null;

    public AuditLoggerProxy(IApplication target) {
        this.target = target;
    }

    @Override
    public String getVersion() {
        System.out.println("audit version");
        return target.getVersion();
    }

    @Override
    public double deposit(String accountId, double amount) {
        System.out.println("Before deposit i can do all the audit");
        double returnValue = target.deposit(accountId,amount);
        System.out.println("After Target call i can do more audit " + returnValue);
        return returnValue+1000;
    }

    @Override
    public double withdraw(String accountiId, double amount) {
        System.out.println("Before Withdraw i can do all the audit then call target ");
        return target.withdraw(accountiId,amount);
    }
}

