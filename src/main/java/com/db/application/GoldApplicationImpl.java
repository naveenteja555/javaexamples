package com.db.application;

public class GoldApplicationImpl  extends ApplicationImpl{
    public GoldApplicationImpl(){
        super();
        System.out.println("Constructor Called");
    }

    @Override
    public String getVersion() {
        return "Gold"+super.getVersion();
    }
}
