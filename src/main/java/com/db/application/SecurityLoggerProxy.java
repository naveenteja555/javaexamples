package com.db.application;

public class SecurityLoggerProxy implements IApplication {
    IApplication target = null;
    private double x=0;

    public SecurityLoggerProxy(IApplication target) {
        this.target = target;
    }

    @Override
    public String getVersion() {
        System.out.println("security get version");
        String x= target.getVersion();

        return x;
    }

    @Override
    public double deposit(String accountId, double amount) {
        System.out.println("Check if user is Authenticated");
        x=target.deposit(accountId, amount);
        System.out.println("Verified");
        System.out.println(x);

        return  x;
    }

    @Override
    public double withdraw(String accountiId, double amount) {
        System.out.println("Before Withdraw i can do all the audit then call target ");
        double d= target.withdraw(accountiId,amount);
        System.out.println(d);
        return d;
    }
    public void multi()
    {
        System.out.println(10 * x);
    }
}