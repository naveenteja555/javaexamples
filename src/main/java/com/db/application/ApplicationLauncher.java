package com.db.application;

public class ApplicationLauncher {
    public static void main(String[] args) {
        /**
         * Disengage with the implementation .....
         * Disengage with the Classes for business implementation only work with interfaces
         * Decoupling will help in enchancement of the future proof coding
         */
        IApplication application1 = ApplicationFactory.getApplication();
        IApplication application2 = ApplicationFactory.getApplication();
        application1.getVersion();
        application2.getVersion();
        application1.deposit("123",500);
        application2.withdraw("123",1000);
    }
}
