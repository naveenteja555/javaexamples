package com.db.lambdafunctions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AirportsFunctionDemo {
    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader("d:\\airportsdata\\airports.csv"));
        String data = "";
        List resultList = new ArrayList();
        int counter = 0;
        while ((data = reader.readLine()) != null) {
            if (counter == 0) {
                counter = counter + 1;
                continue;
            }
            String[] cols = data.split(",");
            if (cols[2].equals("\"heliport\"")) {
                resultList.add(cols);
            }
        }

        List<String[]> helipads = Files.readAllLines(Paths.get("/users/venturinaveenteja/Downloads/airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findHeliPorts).
                collect(Collectors.toList());


        System.out.println(helipads.size());

        List<String[]> smap = Files.readAllLines(Paths.get("/users/venturinaveenteja/Downloads/airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findSmallAirports).
                collect(Collectors.toList());
        System.out.println(smap.size());

        List<String[]> lgap = Files.readAllLines(Paths.get("/users/venturinaveenteja/Downloads/airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findLargeAirports).
                collect(Collectors.toList());
        System.out.println(lgap.size());

        List<String[]> medap = Files.readAllLines(Paths.get("/users/venturinaveenteja/Downloads/airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findMediumAirport).
                collect(Collectors.toList());
        System.out.println(medap.size());

        List<String[]> balport = Files.readAllLines(Paths.get("/users/venturinaveenteja/Downloads/airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findBallonPort).
                collect(Collectors.toList());
        System.out.println(balport.size());

        List<String[]> closeair = Files.readAllLines(Paths.get("/users/venturinaveenteja/Downloads/airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findClosedAirports).
                collect(Collectors.toList());
        System.out.println(closeair.size());
    }

    public static String[] transformByComma(String data) {
        return data.split(",");
    }

    public static boolean findHeliPorts(String[] data) {
        return data[2].equals("\"heliport\"");
    }

    public static boolean findLargeAirports(String[] data) {
        return data[2].equals("\"large_airport\"");
    }

    public static boolean findSmallAirports(String[] data) {
        return data[2].equals("\"small_airport\"");
    }

    public static boolean findMediumAirport(String[] data) {
        return data[2].equals("\"medium_airport\"");
    }

    public static boolean findClosedAirports(String[] data) {
        return data[2].equals("\"closed\"");
    }


    public static boolean findBallonPort(String[] data) {
        return data[2].equals("\"balloonport\"");
    }
    

}
