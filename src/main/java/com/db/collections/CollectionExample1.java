package com.db.collections;

import java.util.*;

public class CollectionExample1 {
    public static void main(String[] args) {
        testLists();
        testSet();
        testMap();
    }
    public static void testLists() {
        List list = new ArrayList();
        for (int i = 0; i <= 10; i++) {
            list.add("ok");
            System.out.println(list.size());
        }
        list.clear();
        List<String> name = new ArrayList<String>();
        name.add("Naveen");
        System.out.println(name.get(0));

    }
    public static void testSet() {
        Set set = new HashSet();
        set.add("VNT");
        set.add("VNT");
        set.add("VNT");
        System.out.println(set.size());

    }
    public static void testMap(){
        Map<String, String> users = new HashMap<String,String>();
        users.put("user1", "xxxxx");
        users.put("user2", "users2");
        System.out.println(users.get("user1"));
    }
}
