package com.db.ioexamples;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReadWriteExample {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        // readFile();
       // readFileUsingReader();
        readFilesNativeWithStreams();
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);
    }

    /**
     * java.io.InputStream is;
     *         java.io.OutputStream os;
     *
     *         java.io.Reader reader;
     *         java.io.Writer writer;
     *
     *         java.io.File file;
     *         java.nio.file.Files nfiles;
     */
    public static void readFile(){
        InputStream is=null;
        try {
            is = new FileInputStream("/users/venturinaveenteja/Desktop/examples/new.txt");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            System.out.println(new String(buffer));
        } catch (Exception e) {
            System.out.println(e);
        }finally {
            if(is!=null){
                try {
                    is.close();
                }catch(IOException e){

                }
            }
        }
    }
    public static void readFileUsingReader() {
        // a varaible to store each line
        String data = "";
        // the buffered reader
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("/users/venturinaveenteja/Downloads/airports.csv"));
            while ((data = reader.readLine()) != null) {
                System.out.println(data);
            }
        } catch (Exception fe) {
            System.out.println(fe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
        public static void readFilesNativeWithStreams() {
            try {
                System.out.println(Files.readString(Paths.get("/users/venturinaveenteja/Downloads/airports.csv")));
            } catch (Exception exception) {
                // exception curation
            } finally {
                // Close your resource if na open
            }

        }
}
